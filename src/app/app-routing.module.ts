import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '@trex/features/home';

import { AuthGuard } from '@trex/core';
import { UnknownRouteComponent } from './unknown-route/unknown-route.component';
import { AppCustomPreloader } from './app-custom-preloader';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { 
    path: 'login', 
    loadChildren: () => import('./features/login/login.module').then(mod => mod.LoginModule),
    data: { preload: true }
  },
  { 
    path: 'register', 
    loadChildren: () => import('./features/register/register.module').then(mod => mod.RegisterModule),
    data: { preload: true }
  },
  {
    path: 'app',
    loadChildren: () => import('./features/app-root/app-root.module').then(mod => mod.AppRootModule),
    canActivate: [AuthGuard],
    data: { preload: true }
  },
  { path: '**', component: UnknownRouteComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: AppCustomPreloader
  })],
  exports: [RouterModule],
  providers: [AppCustomPreloader]
})
export class AppRoutingModule { }
