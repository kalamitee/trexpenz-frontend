import { ValidatorFn, FormGroup, ValidationErrors } from '@angular/forms';

export const matchedPassword: ValidatorFn = (formGroup: FormGroup): ValidationErrors | null => {
  const password = formGroup.get('password');
  const confirmPassword = formGroup.get('_confirmPassword');
  let error = null;

  if (password && confirmPassword) {
    if (password.value && confirmPassword.value) {
      if (password.value !== confirmPassword.value) {
        error = { invalidForm: true };
        password.setErrors({ password: true });
        confirmPassword.setErrors({ confirmPassword: true });
      } else {
        password.setErrors(null);
        confirmPassword.setErrors(null);
      }
    }
  }

  return error;
}
