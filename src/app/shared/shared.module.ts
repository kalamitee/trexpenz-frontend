import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { ModalService } from './components/modal/modal-service/modal.service';

import { 
  CalendarComponent,
  ControlErrorComponent,
  FormDatepickerComponent,
  DatepickerComponent,
  ListComponent,
  ModalComponent,
  UserDetailsComponent
} from './components';

import { 
  FocusContainerDirective,
  FormSubmitDirective,
  ControlErrorContainerDirective,
  ControlErrorsDirective,
  ControlGroupDirective,
  NumberDirective,
  OnSubmitContainerDirective,
  AsyncContainerDirective,
} from './directives';

const COMMON_DIRECTIVES = [
  CalendarComponent,
  ControlErrorComponent,
  FormDatepickerComponent,
  DatepickerComponent,
  ListComponent,
  ModalComponent,
  UserDetailsComponent,

  FocusContainerDirective,
  FormSubmitDirective,
  ControlErrorContainerDirective,
  ControlErrorsDirective,
  ControlGroupDirective,
  NumberDirective,
  OnSubmitContainerDirective,
  AsyncContainerDirective,
];

const ENTRY_COMPONENTS = [
  ControlErrorComponent,
  ModalComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FontAwesomeModule
  ],
  exports: COMMON_DIRECTIVES,
  declarations: COMMON_DIRECTIVES,
  entryComponents: ENTRY_COMPONENTS,
})
export class SharedModule { 
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [
        ModalService
      ]
    };
  }
}
