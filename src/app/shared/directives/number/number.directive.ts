import { Directive, HostListener, ElementRef, Self } from '@angular/core';
import { NgControl } from '@angular/forms';
import { isNil } from 'lodash';

@Directive({
  selector: '[appNumber]'
})
export class NumberDirective {

  private regex: RegExp = new RegExp(/^-?\d+(\.\d+)?$/);
  private specialKeys: Array<number> = [
    37, 38, 39, 40, 13,
    18, 8, 9,
    109, 190, 189
  ];

  constructor(
    private el: ElementRef,
    @Self() private _ngControl: NgControl
  ) {}

  @HostListener('keydown', [ '$event' ])
  onKeyDown(event: KeyboardEvent): void {
    if (event.which === 13) { // convert control value to number if enter key is pressed
      this.onBlur(event);
    }

    if (
      this.specialKeys.indexOf(event.which) !== -1 ||
      this.isCmdKey(event)
    ) {
      return;
    } else {
      this.checkIfStringIsValidNumber(event);
    }
  }

  @HostListener('blur', [ '$event' ])
  onBlur(event): void {
    if (!isNil(this._ngControl.value) && this._ngControl.value !== '') {
      this._ngControl.control.patchValue(Number(this._ngControl.value), { emitEvent: false });
    }

    if (this._ngControl.value === '') {
      this._ngControl.control.patchValue(null, { emitEvent: false });
    }
  }

  private checkIfStringIsValidNumber(event: KeyboardEvent): void {
    const current: string = this.el.nativeElement.value;
    const next: string = current.concat(event.key);

    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }

  private isCmdKey(event: KeyboardEvent): boolean {
    return (event.ctrlKey || event.shiftKey || event.altKey || event.metaKey);
  }
}
