import { Directive, ViewContainerRef, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { RequestDoneStatusService } from '@trex/core';
import { takeUntil } from 'rxjs/operators';

@Directive({
  selector: '[asyncContainer]'
})
export class AsyncContainerDirective implements OnDestroy {

  private _unsubscribe$ = new Subject();

  constructor(
    private _viewContainerRef: ViewContainerRef,
    private _requestDoneStatusService: RequestDoneStatusService,
  ) { 
    this._requestDoneStatusService.requestStatusChange.pipe(
      takeUntil(this._unsubscribe$)
    ).subscribe(isRequestDone => {
      // console.error('DONE ', isRequestDone);
    });
  }

  ngOnDestroy() {
    this._unsubscribe$.next();
    this._unsubscribe$.complete();
  }
}
