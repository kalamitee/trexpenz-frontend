import { Directive, ElementRef, OnInit, OnDestroy, Optional } from '@angular/core';
import { ControlContainer, FormGroupDirective } from '@angular/forms';
import { fromEvent, Subject } from 'rxjs';
import { shareReplay, tap, takeUntil } from 'rxjs/operators';

import { RequestDoneStatusService } from '@trex/core';
import { OnSubmitContainerDirective } from '../on-submit-container/on-submit-container.directive';

@Directive({
  selector: 'form'
})
export class FormSubmitDirective implements OnInit, OnDestroy {

  private _unsubscribe$: Subject<void> = new Subject();

  submit$ = fromEvent(this.element, 'submit').pipe(
    tap(() => {
      if (!this.element.classList.contains('submitted')) {
        this.element.classList.add('submitted');
      }

      this.disableContainer();
      this.toggleFormEnableStatus();
    }),
    shareReplay(1)
  );

  constructor(
    private _host: ElementRef<HTMLFormElement>,
    private _controlContainer: ControlContainer,
    private _requestDoneStatusService: RequestDoneStatusService,
    @Optional() private _onSubmitContainerDirective: OnSubmitContainerDirective
  ) { 
  }

  ngOnInit() {}

  ngOnDestroy() {
    this._unsubscribe$.next();
    this._unsubscribe$.complete();
  }

  private toggleFormEnableStatus() {
    this._requestDoneStatusService.requestStatusChange.pipe(
      takeUntil(this._unsubscribe$)
    ).subscribe(isRequestDone => {
      this.enableContainer();
      this._unsubscribe$.next();
    });
  }

  private disableContainer() {
    if (this.formGroup.valid) {
      if (this._onSubmitContainerDirective) {
        this._onSubmitContainerDirective.disable();
      } else {
        this.disableForm();
      }
    }
  }

  private enableContainer() {
    if (this._onSubmitContainerDirective) {
      this._onSubmitContainerDirective.enable();
    } else {
      this.enableForm();
    }
  }

  private disableForm() {
    this.formGroup.disable();
    this.element.classList.add('disabled-container');
  }

  private enableForm() {
    this.formGroup.enable();
    this.element.classList.remove('disabled-container');
  }

  get element() {
    return this._host.nativeElement;
  }

  get formGroup() {
    const formGroupDirective: FormGroupDirective = <FormGroupDirective> this._controlContainer;
    return formGroupDirective.form;
  }
}
