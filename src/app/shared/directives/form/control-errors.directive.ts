import { Directive, OnInit, OnDestroy, Inject, Optional, Host, ComponentRef, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';
import { NgControl } from '@angular/forms';

import { FORM_ERRORS } from './form-errors';
import { FormSubmitDirective } from './form-submit.directive';
import { ControlErrorContainerDirective } from './control-error-container.directive';
import { ControlGroupDirective } from './control-group.directive';

import { BaseControlErrors } from '../../base/base-control-errors';

@Directive({
  selector: '[formControl], [formControlName]'
})
export class ControlErrorsDirective extends BaseControlErrors implements OnInit, OnDestroy {

  private _controlGroupDirective: ControlGroupDirective;

  constructor(
    private _control: NgControl,
    protected _viewContainerRef: ViewContainerRef,
    protected _componentFactoryResolver: ComponentFactoryResolver,
    @Optional() controlErrorContainer: ControlErrorContainerDirective,
    @Optional() @Host() protected form: FormSubmitDirective,
    @Inject(FORM_ERRORS) protected errors,
    @Optional() @Host() controlGroupDirective: ControlGroupDirective
  ) { 
    super(_viewContainerRef, _componentFactoryResolver, controlErrorContainer, form, errors);
    this._controlGroupDirective = controlGroupDirective;
  }

  ngOnInit() {
    if (this._controlGroupDirective) {
      this._controlGroupDirective.setControl(this._control);
    } else {
      this.subscribeToEvents(this._control);
    }
  }
}
