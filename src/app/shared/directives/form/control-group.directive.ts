import { Directive, OnDestroy, Inject, Optional, Host, ViewContainerRef, ComponentFactoryResolver } from '@angular/core';

import { FORM_ERRORS } from './form-errors';
import { FormSubmitDirective } from './form-submit.directive';
import { BaseControlErrors } from '../../base/base-control-errors';
import { ControlErrorContainerDirective } from './control-error-container.directive';

@Directive({
  selector: '[controlGroup]'
})
export class ControlGroupDirective extends BaseControlErrors implements OnDestroy {

  private _control;

  constructor(
    protected _viewContainerRef: ViewContainerRef,
    protected _componentFactoryResolver: ComponentFactoryResolver,
    @Optional() controlErrorContainer: ControlErrorContainerDirective,
    @Optional() @Host() protected form: FormSubmitDirective,
    @Inject(FORM_ERRORS) protected errors,
  ) {
    super(_viewContainerRef, _componentFactoryResolver, controlErrorContainer, form, errors);
  }

  setControl(control) {
    if (!this._control) {
      this._control = control;
      this.subscribeToEvents(this._control);
    }
  }
}
