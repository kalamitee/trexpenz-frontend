import { InjectionToken } from '@angular/core';

export const defaultErrors = {
  required: (error) => 'This field is required',
  email: (error) => 'Invalid email format',
  password: (error) => 'Password does not match confirm password',
  confirmPassword: (error) => 'Confirm password does not match password'
};

export const FORM_ERRORS = new InjectionToken('FORM_ERRORS', {
  providedIn: 'root',
  factory: () => defaultErrors
});
