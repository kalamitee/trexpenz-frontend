import { Directive, ElementRef, OnInit } from '@angular/core';
import { FocusTrapFactory } from '@angular/cdk/a11y';

@Directive({
  selector: '[appFocusContainer]'
})
export class FocusContainerDirective implements OnInit {

  constructor(
    private _host: ElementRef<HTMLElement>,
    private _focusTrap: FocusTrapFactory
  ) { }

  ngOnInit(): void {
    let focusTrap = this._focusTrap.create(this.container);
    focusTrap.focusFirstTabbableElement();
  }

  get container() {
    return this._host.nativeElement;
  }
}
