import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appOnSubmitContainer]'
})
export class OnSubmitContainerDirective {

  constructor(
    private _host: ElementRef<HTMLFormElement>,
  ) { }

  disable() {
    this.element.classList.add('disabled-container');
  }

  enable() {
    this.element.classList.remove('disabled-container');
  }

  get element() {
    return this._host.nativeElement;
  }
}
