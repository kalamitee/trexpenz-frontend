import { Component, OnInit, Input, Output, EventEmitter, OnChanges } from '@angular/core';
import { BaseDate } from '../calendar/base-date';
import { CalendarDate } from '../calendar/calendar-date.interface';

@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss']
})
export class DatepickerComponent extends BaseDate implements OnInit, OnChanges {

  @Input() selectedDate;
  @Output() dateSelected = new EventEmitter<CalendarDate>();

  constructor() { 
    super();
  }

  ngOnInit() {
    this.generateCalendar();
  }

  selectDate(date: CalendarDate): void {
    this.setHighlightedDate(date);
    this.dateSelected.emit(date);
  }
}
