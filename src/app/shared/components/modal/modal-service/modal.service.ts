import { Injectable, ComponentFactoryResolver, Injector, Inject, ApplicationRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';

import { ModalComponent } from '../modal.component';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  private _modalRef: any;
  private _childComponentRef: any;
  private _modalConfig: any;
  private _modalData: any;

  constructor(
    private _appRef: ApplicationRef,
    private resolver : ComponentFactoryResolver,
    private injector: Injector,
    @Inject(DOCUMENT) private document: Document
  ) {}

  setConfig(modalConfig) {
    this._modalConfig = modalConfig;
  }

  setModalData(data) {
    this._modalData = data;
  }

  open() {
    const factory = this.resolver.resolveComponentFactory(ModalComponent);
    const ngContent = this.resolveNgContent(this._modalConfig.component);
    const componentRef = factory.create(this.injector, ngContent);
    
    this._modalRef = componentRef;

    this._modalRef.instance.setTitle(this._modalConfig.title);
    componentRef.hostView.detectChanges();
 
    const { nativeElement } = componentRef.location;

    this.document.body.appendChild(nativeElement);
  }
 
  resolveNgContent(content) {
    const factory = this.resolver.resolveComponentFactory(content);
    const componentRef = factory.create(this.injector);

    this._childComponentRef = componentRef;
    this._childComponentRef.instance.setData(this._modalData);
    
    this._appRef.attachView(componentRef.hostView);

    return [[componentRef.location.nativeElement]];
  }

  close() {
    if (this._childComponentRef) {
      this._childComponentRef.destroy();
    }
    
    this._appRef.detachView(this._modalRef.hostView);
    this._modalRef.destroy();
  }
}
