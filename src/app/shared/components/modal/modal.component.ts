import { Component, OnInit, Inject, forwardRef } from '@angular/core';
import * as _ from 'lodash';

import { ModalService } from './modal-service/modal.service';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {

  modalTitle: string = '';

  constructor(
    @Inject(forwardRef(() => ModalService)) private _modalService
  ) { }

  ngOnInit() {
  }

  close() {
    this._modalService.close();
  }

  setTitle(title) {
    this.modalTitle = title;
  }
}
