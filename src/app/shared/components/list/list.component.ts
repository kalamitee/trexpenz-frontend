import { Component, OnInit, Input, TemplateRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() listDataTemplate: TemplateRef<any>;
  @Input() listData;

  @Output() dataClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  dataClicked(event, data, index) {
    event.stopPropagation();
    this.dataClick.emit({data: data, index: index });
  }
}
