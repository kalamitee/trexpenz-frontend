import * as moment from 'moment';
import * as _ from 'lodash';

import { CalendarDate } from './calendar-date.interface';
import { Utils } from '../../utils/utils';

export class BaseDate {

  static MIN_YEAR = 10;
  static MAX_YEAR = 10;

  highlightedDate;
  selectedDate;

  currentDate = moment();
  currentYear = this.currentDate.year();
  currentMonth = this.currentDate.format('MMM');

  dayNames = ['S', 'M', 'T', 'W', 'T', 'F', 'S'];
  months = _.values(Utils.MONTHS);
  years = _.range(this.currentYear - BaseDate.MIN_YEAR, this.currentYear + BaseDate.MAX_YEAR);

  listActive = {
    month: false,
    year: false
  };

  weeks: CalendarDate[][] = [];

  ngOnChanges(changes) {
    if (changes.selectedDate && changes.selectedDate.currentValue) {
      this.selectedDate = moment(changes.selectedDate.currentValue);
      this.currentDate = moment(changes.selectedDate.currentValue);
      this.updateYearAndMonth();
      this.generateCalendar();
    }
  }

  generateCalendar(): void {
    const dates = this.fillDates(this.currentDate);
    const weeks: CalendarDate[][] = [];
    while (dates.length > 0) {
      weeks.push(dates.splice(0, 7));
    }
    this.weeks = weeks;
  }

  fillDates(currentMoment: moment.Moment): CalendarDate[] {
    const firstOfMonth = moment(currentMoment).startOf('month').day();
    const firstDayOfGrid = moment(currentMoment).startOf('month').subtract(firstOfMonth, 'days');
    const start = firstDayOfGrid.date();
    const monthDays = _.range(start, start + 42);
    const monthDaysConfig = monthDays.map((monthDay) => {
      return this.configureMonthDay(monthDay, firstDayOfGrid);
    });

    return monthDaysConfig;
  }

  private configureMonthDay(monthDay, firstDayOfGrid) {
    const d = moment(firstDayOfGrid).date(monthDay);
    const dateData = {
      today: this.isToday(d),
      selected: this.isSelected(d),
      mDate: d
    };

    if (dateData.selected) {
      this.highlightedDate = dateData;
    }

    return dateData;
  }

  isToday(date: moment.Moment): boolean {
    return moment().isSame(moment(date), 'day');
  }

  isSelectedMonth(date: moment.Moment): boolean {
    return moment(date).isSame(this.currentDate, 'month');
  }

  prevMonth(event): void {
    event.stopPropagation();

    this.currentDate = moment(this.currentDate).subtract(1, 'months');
    this.updateYearAndMonth();
    this.deactivateLists(null);
    this.generateCalendar();
  }

  nextMonth(event): void {
    event.stopPropagation();

    this.currentDate = moment(this.currentDate).add(1, 'months');
    this.updateYearAndMonth();
    this.deactivateLists(null);
    this.generateCalendar();
  }

  isSelected(date: moment.Moment): boolean {
    if (this.selectedDate) {
      return moment(date).isSame(this.selectedDate, 'day');
    }

    return false;
  }

  setHighlightedDate(date) {
    if (this.highlightedDate) {
      this.highlightedDate.selected = false;
    }

    this.highlightedDate = date;
    date.selected = true;
  }

  updateMonth(data) {
    this.currentDate.set('month', data.index);
    this.currentMonth = this.currentDate.format('MMM');
    this.generateCalendar();
    this.listActive.month = false;
  }

  updateYear(data) {
    this.currentDate.set('year', data.data);
    this.currentYear = this.currentDate.year();
    this.generateCalendar();
    this.listActive.year = false;
  }

  toggleList(event, listActive, listName) {
    event.stopPropagation();
    this.deactivateLists(listName);
    listActive[listName] = !listActive[listName];
  }

  deactivateLists(listNameToExclude) {
    for (const listName in this.listActive) {
      if (listName !== listNameToExclude) {
        this.listActive[listName] = false;
      }
    }
  }

  updateYearAndMonth() {
    this.currentMonth = this.currentDate.format('MMM');
    this.currentYear = this.currentDate.year();
  }
}
