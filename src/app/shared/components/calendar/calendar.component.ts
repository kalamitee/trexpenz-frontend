import { Component, EventEmitter, Input, OnInit, Output, TemplateRef } from '@angular/core';

import { BaseDate } from './base-date';
import { CalendarDate } from './calendar-date.interface';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent extends BaseDate implements OnInit {

  @Input() dayViewTemplate: TemplateRef<any>;
  @Output() onSelectDate = new EventEmitter<CalendarDate>();
  @Output() yearChange = new EventEmitter();

  dayNames = ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'];

  constructor() {
    super();
  }

  ngOnInit(): void {
    this.generateCalendar();
  }

  selectDate(date: CalendarDate): void {
    this.setHighlightedDate(date);
    this.onSelectDate.emit(date);
  }
}
