import { Component, OnInit, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';
import { fromEvent } from 'rxjs';
import * as moment from 'moment';

export const FORM_DATEPICKER_VALUE_ACCESSOR = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => FormDatepickerComponent),
  multi: true
};

@Component({
  selector: 'app-form-datepicker',
  templateUrl: './form-datepicker.component.html',
  styleUrls: ['./form-datepicker.component.scss'],
  providers: [FORM_DATEPICKER_VALUE_ACCESSOR]
})
export class FormDatepickerComponent implements OnInit, ControlValueAccessor{

  private _documentClick$ = fromEvent(document, 'click');

  innerValue;
  onChange;
  subscription;

  selectedDate;
  calendarActive = false;

  constructor() { }

  ngOnInit() {
  }

  writeValue(value: any) {
    if (value) {
      this.updateValues(value);
    } else {
      this.innerValue = null;
    }
  }

  registerOnChange(fn: any) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {

  }

  setDisabledState(isDisabled: boolean) {
    
  }

  toggleCalendar() {
    this.calendarActive = !this.calendarActive;

    if (this.calendarActive) {
      setTimeout(() => {
        this.subscription = this._documentClick$.subscribe(res => {
          this.toggleCalendar();
        });
      });
    } else {
      this.subscription.unsubscribe();
    }
  }

  onSelectDate(dateSelected) {
    const stringifyDate = dateSelected.mDate.format('YYYY-MM-DD');
    this.updateValues(stringifyDate);
    this.onChange(stringifyDate);
  }

  updateValues(date) {
    this.innerValue = moment(date).format('MM/DD/YYYY');
    this.selectedDate = date;
  }

  parseInput(event) {
    this.onChange(event.target.value);
  }
}
