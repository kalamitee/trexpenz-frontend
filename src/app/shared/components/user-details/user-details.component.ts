import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Validators } from '@angular/forms';
import * as _ from 'lodash';

import { FormService } from '@trex/core';
import { matchedPassword } from '@trex/shared/utils';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.scss']
})
export class UserDetailsComponent implements OnInit {

  @Input() accountFieldsShown: boolean = true;
  @Input() data = {};
  @Input() saveButtonLabel = 'Register';

  @Output() save = new EventEmitter();
  @Output() cancel = new EventEmitter();

  formConfig = {
    controls: [
      { name: 'firstName', validators: Validators.required },
      { name: 'lastName' },
      { name: 'email', validators: [Validators.required, Validators.email] },
    ],
    validators: [matchedPassword]
  };

  private _accountFields = [
    { name: 'username', validators: Validators.required },
    { name: 'password', validators: Validators.required },
    { name: '_confirmPassword', validators: Validators.required }
  ];

  detailsForm;

  constructor(
    private _formService: FormService
  ) { }

  ngOnInit() {
    if (this.accountFieldsShown) {
      this.formConfig.controls.push(...this._accountFields);
    }
    this.detailsForm = this._formService.generateFormGroup(this.formConfig, this.data);
  }

  saveEmit(formValue) {
    if (this.accountFieldsShown) {
      formValue.account = {
        username: formValue.username,
        password: formValue.password
      };
    }

    const updatedData = _.merge(this.data, formValue);

    if (this.detailsForm.valid) {
      this.save.emit(updatedData);
    }
  }

  onCancel() {
    this.cancel.emit();
  }
}
