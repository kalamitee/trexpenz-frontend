import { Utils } from '../utils/utils';
import * as _ from 'lodash';

export class BaseExpenseTypeSummary {
  expenseTypes;
  expenses;

  sortedConfiguredExpenseTypes;
  months = _.values(Utils.MONTHS);

  ngOnInit() {
    this.init();
  }

  ngOnChanges(changes) {
    if (changes.expenses && !changes.expenses.firstChange) {
      this.init();
    }
  }

  private init() {
    const expensesGroup = _.groupBy(this.expenses, 'expenseType.id');
    const expensesSumByDate = _.mapValues(expensesGroup, (value) => {
      return _.sumBy(value, 'amount');
    });

    const configuredExpenseTypes = this.expenseTypes.map(etItem => {
      etItem.totalExpenses = expensesSumByDate[etItem.id] || 0;
      etItem.monthTotal = this.getMonthTotal(expensesGroup[etItem.id]);

      return etItem;
    });

    this.sortedConfiguredExpenseTypes = _.orderBy(configuredExpenseTypes, ['totalExpenses'], ['desc']);
  }

  private getMonthTotal(expenseGroup) {
    const monthTotal = this.months.map((month, ind) => {
      return this.expenseGroupMonthTotal(expenseGroup, ind);
    });

    return monthTotal;
  }

  private expenseGroupMonthTotal(expenseGroup, month) {
    if (expenseGroup) {
      const filteredExpenseGroup = this.filterExpenseGroupByMonth(expenseGroup, month);
      return _.sumBy(filteredExpenseGroup, 'amount');
    }

    return 0;
  }

  private filterExpenseGroupByMonth(expenseGroup, month) {
    return expenseGroup.filter(egItem => {
      return (egItem.budgetBreakdown.month - 1) === month;
    });
  }
}
