import { ViewContainerRef, ComponentFactoryResolver, ComponentRef, HostBinding } from '@angular/core';
import { takeUntil } from 'rxjs/operators';
import { Observable, Subject, EMPTY, merge } from 'rxjs';

import { ControlErrorContainerDirective } from '../directives/form/control-error-container.directive';
import { FormSubmitDirective } from '../directives/form/form-submit.directive';
import { ControlErrorComponent } from '../components/custom-form-controls/control-error/control-error.component';

export class BaseControlErrors {
  
  @HostBinding('class.invalid') private _invalid = false;

  private _unsubscribe$ = new Subject();
  private _submit$: Observable<Event>;

  ref: ComponentRef<ControlErrorComponent>;
  container: ViewContainerRef;

  constructor(
    protected viewContainerRef: ViewContainerRef,
    protected componentFactoryResolver: ComponentFactoryResolver,
    protected controlErrorContainer: ControlErrorContainerDirective,
    protected form: FormSubmitDirective,
    protected errors
  ) {
    this._submit$ = form ? form.submit$ : EMPTY;
    this.container = controlErrorContainer ? controlErrorContainer.viewContainerRef : viewContainerRef;
  }

  subscribeToEvents(control) {
    merge(
      this._submit$,
      // control.valueChanges,
      control.statusChanges
    ).pipe(
      takeUntil(this._unsubscribe$)
    ).subscribe(() => {
      const controlErrors = control.errors;

      if (controlErrors) {
        const firstKey = Object.keys(controlErrors)[0];
        const getError = this.errors[firstKey];
        const text = getError(controlErrors[firstKey]);
        this._invalid = true;
        this.setError(text);
      } else if (this.ref) {
        this._invalid = false;
        this.setError(null);
      }
    });
  }

  ngOnDestroy() {
    this._unsubscribe$.next();
    this._unsubscribe$.complete();
  }

  setError(text: string) {
    if (!this.ref) {
      const factory = this.componentFactoryResolver.resolveComponentFactory(ControlErrorComponent);
      this.ref = this.container.createComponent(factory);
    }

    this.ref.instance.text = text;
  }
}
