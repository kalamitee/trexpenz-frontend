import { Injectable, ComponentFactoryResolver, Injector, Inject, TemplateRef, Type, ApplicationRef } from '@angular/core';
import { DOCUMENT } from '@angular/common';

// import { ModalComponent } from '../../components/modal/modal.component';

export type Content<T> = string | TemplateRef<T> | Type<T>;

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  // _modalRef: any;

  // constructor(
  //   private _appRef: ApplicationRef,
  //   private resolver : ComponentFactoryResolver,
  //   private injector: Injector,
  //   @Inject(DOCUMENT) private document: Document
  // ) { }

  // open<T>(content: Content<T>) {
  //   const factory = this.resolver.resolveComponentFactory(ModalComponent);
  //   const ngContent = this.resolveNgContent(content);
  //   const componentRef = factory.create(this.injector, ngContent);
    
  //   this._modalRef = componentRef;
  //   // componentRef.hostView.detectChanges();
 
  //   const { nativeElement } = componentRef.location;

  //   this.document.body.appendChild(nativeElement);
  // }
 
  // resolveNgContent<T>(content: Content<T>) {
  //   if (typeof content === 'string') {
  //     const element = this.document.createTextNode(content);
  //     return [[element]];
  //   }
 
  //   if (content instanceof TemplateRef) {
  //     const viewRef = content.createEmbeddedView(null);
  //     return [viewRef.rootNodes];
  //   }
 
  //   const factory = this.resolver.resolveComponentFactory(content);
  //   const componentRef = factory.create(this.injector);
    
  //   this._appRef.attachView(componentRef.hostView);

  //   return [[componentRef.location.nativeElement]];
  // }

  // close() {
  //   this._appRef.detachView(this._modalRef.hostView);
  //   this._modalRef.destroy();
  // }
}
