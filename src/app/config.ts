import { HttpHeaders } from '@angular/common/http';
import { environment } from '../environments/environment';

const baseUrl = environment.baseUrl;
const apiVersion = 'v1';

export const apiUrl = `${baseUrl}${apiVersion}/`;
export const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
