import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(
    private _toastrService: ToastrService
  ) { }

  success(message, title = null) {
    this._toastrService.success(message, title);
  }

  error(message, title = null) {
    this._toastrService.error(message, title);
  }
}
