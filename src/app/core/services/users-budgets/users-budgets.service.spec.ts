import { TestBed } from '@angular/core/testing';

import { UsersBudgetsService } from './users-budgets.service';

describe('UsersBudgetsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersBudgetsService = TestBed.get(UsersBudgetsService);
    expect(service).toBeTruthy();
  });
});
