import { TestBed } from '@angular/core/testing';

import { CredService } from './cred.service';

describe('CredService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CredService = TestBed.get(CredService);
    expect(service).toBeTruthy();
  });
});
