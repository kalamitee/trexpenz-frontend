import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class CredService extends BaseService {

  endpoint = 'cred';

  constructor(
    protected _http: HttpClient
  ) { 
    super(_http);
  }

  transformPayload(payload) {
    return payload;
  }
}
