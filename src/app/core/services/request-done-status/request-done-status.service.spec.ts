import { TestBed } from '@angular/core/testing';

import { RequestDoneStatusService } from './request-done-status.service';

describe('RequestDoneStatusService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestDoneStatusService = TestBed.get(RequestDoneStatusService);
    expect(service).toBeTruthy();
  });
});
