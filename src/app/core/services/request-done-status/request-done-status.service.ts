import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RequestDoneStatusService {

  requestStatusChange: Observable<any>;

  private _requestStatus: Subject<any>;

  constructor() { 
    this._requestStatus = new Subject();
    this.requestStatusChange = this._requestStatus.asObservable();
  }

  startRequest() {
    this._requestStatus.next(false);
  }

  endRequest() {
    this._requestStatus.next(true);
  }
}
