import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService extends BaseService {

  endpoint = 'auth';

  constructor(
    protected _http: HttpClient
  ) { 
    super(_http);
  }

  login(credentials: { username: string, password: string }) {
    this.create(credentials);
  }

  logout() {
    this.delete();
  }

  hasSavedToken() {
    return !!localStorage.getItem('token');
  }

  transformPayload(payload) {
    return payload;
  }

  setUserConfig(userData) {
    localStorage.setItem('token', userData.auth_token);
    localStorage.setItem('user', JSON.stringify(userData.user));
  }

  removeUserConfig() {
    localStorage.removeItem('token');
    localStorage.removeItem('user');
  }

  getLoggedInUser() {
    return JSON.parse(localStorage.getItem('user'));
  }
}
