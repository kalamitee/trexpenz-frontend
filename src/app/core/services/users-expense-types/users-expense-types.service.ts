import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../base/base.service';
import { LogoutService } from '../logout/logout.service';

@Injectable({
  providedIn: 'root'
})
export class UsersExpenseTypesService extends BaseService {

  constructor(
    protected _http: HttpClient,
    protected _logoutService: LogoutService,
  ) { 
    super(_http);
    this._logoutService.logout.subscribe(() => this.emptyDataStore());
  }

  getEndpoint() {
    const _loggedInUser: any = JSON.parse(localStorage.getItem('user'));
    return `users/${_loggedInUser.id}/expense-types`;
  }
}
