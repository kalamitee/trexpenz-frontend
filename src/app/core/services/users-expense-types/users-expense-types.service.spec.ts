import { TestBed } from '@angular/core/testing';

import { UsersExpenseTypesService } from './users-expense-types.service';

describe('UsersExpenseTypesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersExpenseTypesService = TestBed.get(UsersExpenseTypesService);
    expect(service).toBeTruthy();
  });
});
