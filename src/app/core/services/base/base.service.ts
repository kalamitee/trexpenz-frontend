import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';

import { apiUrl, httpOptions } from '../../../config';

@Injectable({
  providedIn: 'root'
})
export class BaseService {

  endpoint;

  data$: Observable<any[]>;
  
  private _data: Subject<any>;
  private _dataStore: {
    data: any[]
  };

  constructor(
    protected _http: HttpClient
  ) { 
    this._dataStore = { data: [] };
    this._data = new Subject();
    this.data$ = this._data.asObservable();
  }

  save(data): void {
    if (!data.id) {
      this.create(data);
    } else {
      this.update(data);
    }
  }

  create(data): void {
    this._http.post<any>(`${apiUrl}${this.getEndpoint()}`, this.transformPayload(data), httpOptions).subscribe(res => {
      this._dataStore.data.push(res.data);
      this._data.next(Object.assign({}, this._dataStore).data);
    });
  }

  read(q: any = null): void {
    let url = `${apiUrl}${this.getEndpoint()}`;

    if (q) {
      url += `?q=${encodeURIComponent(q)}`;
    }

    this._http.get<any>(`${url}`, httpOptions).subscribe(res => {
      this._dataStore.data = res.data;
      this._data.next(Object.assign({}, this._dataStore).data);
    });
  }

  readOne(id: number): void {
    this._http.get<any>(`${apiUrl}${this.getEndpoint()}/${id}`, httpOptions).subscribe(res => {
      let notFound = true;

      this._dataStore.data.forEach((item, index) => {
        if (item.id === res.data.id) {
          this._dataStore.data[index] = res.data;
          notFound = false;
        }
      });

      if (notFound) {
        this._dataStore.data.push(res.data);
      }

      this._data.next(Object.assign({}, this._dataStore).data);
    });
  }

  update(data): void {
    this._http.put<any>(`${apiUrl}${this.getEndpoint()}`, this.transformPayload(data), httpOptions).subscribe(res => {
      this._dataStore.data.forEach((t, i) => {
        if (t.id === data.id) { 
          this._dataStore.data[i] = res.data; 
        }
      });

      this._data.next(Object.assign({}, this._dataStore).data);
    });
  }

  delete(id: number = null): void {
    let url = `${apiUrl}${this.getEndpoint()}`;

    if (id) {
      url += `/${id}`;
    }

    this._http.delete<any>(`${url}`).subscribe(res => {
      if (id) {
        this._dataStore.data.forEach((d, i) => {
          if (d.id === id) { 
            this._dataStore.data.splice(i, 1); 
          }
        });
      } else {
        this._dataStore.data.splice(0, 1);
      }

      this._data.next(Object.assign({}, this._dataStore).data);
    });
  }

  readDataFromCache() {
    if (this._dataStore.data.length) {
      setTimeout(() => {
        this._data.next(Object.assign({}, this._dataStore).data);
      });
    } else {
      this.read();
    }
  }

  emptyDataStore() {
    this._dataStore.data = [];
  }

  getEndpoint() {
    return this.endpoint;
  }

  transformPayload(payload: any): any {
    const user = JSON.parse(localStorage.getItem('user'));
    payload.user = { id: user.id };

    return { data: payload };
  }
}
