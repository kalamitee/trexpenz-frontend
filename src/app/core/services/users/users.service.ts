import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class UsersService extends BaseService {

  endpoint = 'users';

  constructor(
    protected _http: HttpClient
  ) { 
    super(_http);
  }

  transformPayload(payload) {
    return { data: payload };
  }
}
