import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { BaseService } from '../base/base.service';

@Injectable({
  providedIn: 'root'
})
export class UsersExpensesService extends BaseService {

  constructor(
    protected _http: HttpClient
  ) { 
    super(_http);
  }

  getEndpoint() {
    const _loggedInUser: any = JSON.parse(localStorage.getItem('user'));
    return `users/${_loggedInUser.id}/expenses`;
  }
}
