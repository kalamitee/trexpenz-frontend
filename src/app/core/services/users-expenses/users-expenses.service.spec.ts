import { TestBed } from '@angular/core/testing';

import { UsersExpensesService } from './users-expenses.service';

describe('UsersExpensesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UsersExpensesService = TestBed.get(UsersExpensesService);
    expect(service).toBeTruthy();
  });
});
