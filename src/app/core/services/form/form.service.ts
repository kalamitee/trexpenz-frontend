import { Injectable } from '@angular/core';
import { FormBuilder, FormControl } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormService {

  constructor(
    private _formBuilder: FormBuilder
  ) { }

  generateFormGroup(formConfig, data) {
    const group = {};

    formConfig.controls.forEach((control) => {
      if (control.type === 'array') {
        group[control.name] = this.generateFormArray(control, data[control.name]);
      } else {
        const newControl = new FormControl(data[control.name]);
        if (control.validators) {
          newControl.setValidators(Array.isArray(control.validators) ? control.validators : [control.validators]);
        }

        if (control.valueChanges) {
          newControl.valueChanges.subscribe(control.valueChanges);
        }
        
        group[control.name] = newControl;
      }
    });

    return this._formBuilder.group(group, { validators: formConfig.validators });
  }

  generateFormArray(control, data) {
    let formGroups = [];

    data.forEach((d) => {
      formGroups.push(this.generateFormGroup(control, d));
    });
    
    const formArray = this._formBuilder.array(formGroups);
    if (control.valueChanges) {
      formArray.valueChanges.subscribe(control.valueChanges);
    }
    
    return formArray;
  }
}
