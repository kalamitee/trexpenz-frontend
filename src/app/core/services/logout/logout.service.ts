import { Injectable } from '@angular/core';
import { Observable, Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LogoutService {

  logout: Observable<any>;
  private _logout: Subject<any>;

  constructor() { 
    this._logout = new Subject();
    this.logout = this._logout.asObservable();
  }

  loggedOut() {
    this._logout.next();
  }
}
