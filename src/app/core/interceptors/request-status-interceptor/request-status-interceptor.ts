import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { RequestDoneStatusService } from '../../services/request-done-status/request-done-status.service';

@Injectable({
  providedIn: 'root'
})
export class RequestStatusInterceptor implements HttpInterceptor {

  constructor(
    private _requestDoneStatusService: RequestDoneStatusService
  ) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this._requestDoneStatusService.startRequest();

    return next.handle(req).pipe(
      tap(res => {
        this._requestDoneStatusService.endRequest();
      })
    );
  }
}
