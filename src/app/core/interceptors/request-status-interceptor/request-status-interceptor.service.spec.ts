import { TestBed } from '@angular/core/testing';

import { RequestStatusInterceptor } from './request-status-interceptor';

describe('RequestStatusInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RequestStatusInterceptor = TestBed.get(RequestStatusInterceptor);
    expect(service).toBeTruthy();
  });
});
