import { HTTP_INTERCEPTORS } from '@angular/common/http';

import { TokenInterceptor } from './token-interceptor/token-interceptor';
import { RequestStatusInterceptor } from './request-status-interceptor/request-status-interceptor';

/** Http interceptor providers in outside-in order */
export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
  { provide: HTTP_INTERCEPTORS, useClass: RequestStatusInterceptor, multi: true },
];
