import { ErrorHandler, Injectable, Injector, NgZone } from '@angular/core';
import { Router } from '@angular/router';
import { HttpErrorResponse } from '@angular/common/http';
import * as StackTraceParser from 'stacktrace-js';

import { NotificationService } from '../services/notification/notification.service';
import { AuthService } from '../services/auth/auth.service';
import { RequestDoneStatusService } from '../services/request-done-status/request-done-status.service';
import { LogoutService } from '../services/logout/logout.service';

@Injectable()
export class ExceptionHandler implements ErrorHandler {
  notificationService;
  authService;
  logoutService;
  requestDoneStatusService;

  constructor(private _injector: Injector, private _ngZone: NgZone) {
    this.notificationService = this._injector.get(NotificationService);
    this.authService = this._injector.get(AuthService);
    this.logoutService = this._injector.get(LogoutService);
    this.requestDoneStatusService = this._injector.get(RequestDoneStatusService);
  }

  handleError(error: Error | HttpErrorResponse) {
    if (error instanceof HttpErrorResponse) {
      this.handleServerError(error);
    } /*else if(!navigator.onLine) {  // TODO: Uncomment once done in production
      this.requestDoneStatusService.endRequest();
      this.notificationService.error('No internet connection');
    } */ else {
      console.error('The error ', error);
    }
  }

  handleServerError(error: HttpErrorResponse) {
    if (error.status === 401) {
      if (error.error && error.error.message) {
        if (error.error.message.toLowerCase() === 'token is expired') {
          this.authService.emptyDataStore();
          this.authService.removeUserConfig();
          this.logoutService.loggedOut();

          this._ngZone.run(() => {
            const router = this._injector.get(Router);
            router.navigate(['/login']);
          });
        }
      }
    }
    
    this.requestDoneStatusService.endRequest();
    this.notificationService.error(this.getErrorMessage(error), `Error: ${error.status}`);
  }

  getErrorMessage(error) {
    let errMsg = null;

    if (error.error.message || error.error.error) {
      errMsg = error.error.message || error.error.error;
    } else {
      errMsg = error.error;
    }

    return errMsg;
  }
}
