import { RouteReuseStrategy, ActivatedRouteSnapshot, DetachedRouteHandle } from '@angular/router';

export class AppReuseStrategy implements RouteReuseStrategy {

  static willReuse = false;

  private acceptedRoutes: string[] = ['budgets', 'expense-types', 'expenses', 'dashboard'];
  storedRoutes: { [key: string]: DetachedRouteHandle } = {};
    
  shouldReuseRoute(future: ActivatedRouteSnapshot, curr: ActivatedRouteSnapshot): boolean {
    return future.routeConfig === curr.routeConfig;
  }

  shouldDetach(route: ActivatedRouteSnapshot): boolean {
    const parentRoute = route.parent.routeConfig ? route.parent.routeConfig.path : null;

    if (parentRoute && (this.acceptedRoutes.indexOf(parentRoute) > -1) && AppReuseStrategy.willReuse) {
      AppReuseStrategy.willReuse = false;
      return true;
    } else {
      return false;
    }
  }

  store(route: ActivatedRouteSnapshot, handle: DetachedRouteHandle): void {
    this.storedRoutes[route.parent.routeConfig.path] = handle;
  }

  shouldAttach(route: ActivatedRouteSnapshot): boolean {
    let canAttach = false;
    const isRouteStored = !!route.parent.routeConfig && !!this.storedRoutes[route.parent.routeConfig.path];

    if (isRouteStored && AppReuseStrategy.willReuse) {
      canAttach = true;
      AppReuseStrategy.willReuse = false;
    }

    return canAttach;
  }

  retrieve(route: ActivatedRouteSnapshot): DetachedRouteHandle {
    if (!route.parent.routeConfig) {
      return null;
    }
    
    return this.storedRoutes[route.parent.routeConfig.path];
  }
}
