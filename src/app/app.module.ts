import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, ErrorHandler } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { RouteReuseStrategy } from '@angular/router';

import { ToastrModule } from 'ngx-toastr';
import { NgProgressModule } from '@ngx-progressbar/core';
import { NgProgressHttpModule } from '@ngx-progressbar/http';

import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { 
  faTachometerAlt, 
  faHandHoldingUsd, 
  faMoneyBillWave,
  faListAlt,
  faUserCircle,
  faWallet,
  faChevronLeft,
  faChevronRight,
  faPlus,
  faMinus,
  faTimes,
  faCaretLeft,
  faCaretRight,
  faTrashAlt,
  faUser,
  faKey,
  faBan,
  faExclamationTriangle,
  faList,
} from '@fortawesome/free-solid-svg-icons';

import { AppRoutingModule } from './app-routing.module';

import { CoreModule, httpInterceptorProviders } from './core';
import { SharedModule } from './shared';
import { HomeModule } from './features/home';

import { AppComponent } from './app.component';
import { AppReuseStrategy } from './app-reuse-strategy';
import { ExceptionHandler } from '@trex/core';
import { UnknownRouteComponent } from './unknown-route/unknown-route.component';

@NgModule({
  declarations: [
    AppComponent,
    UnknownRouteComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot({
      positionClass: 'toast-top-center',
      maxOpened: 4,
      preventDuplicates: true,
      tapToDismiss: true,
      autoDismiss: true,
      onActivateTick: true,
      timeOut: 2000
    }),
    HttpClientModule,
    NgProgressModule.withConfig({
      spinner: false,
      meteor: false
    }),
    NgProgressHttpModule.withConfig({
      id: 'http'
    }),
    FontAwesomeModule,
    
    AppRoutingModule,
    SharedModule.forRoot(),
    CoreModule,

    HomeModule,
  ],
  providers: [
    httpInterceptorProviders,
    {
      provide: RouteReuseStrategy,
      useClass: AppReuseStrategy
    },
    {
      provide: ErrorHandler,
      useClass: ExceptionHandler
    }
  ],
  bootstrap: [
    AppComponent
  ]
})
export class AppModule { 
  constructor() {
    library.add(
      faTachometerAlt, 
      faHandHoldingUsd, 
      faMoneyBillWave,
      faListAlt,
      faUserCircle,
      faWallet,
      faChevronLeft,
      faChevronRight,
      faPlus,
      faMinus,
      faTimes,
      faCaretLeft,
      faCaretRight,
      faTrashAlt,
      faUser,
      faKey,
      faBan,
      faExclamationTriangle,
      faList,
    );
  }
}
