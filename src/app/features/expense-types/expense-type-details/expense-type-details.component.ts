import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';
import * as _ from 'lodash';

import { UsersExpenseTypesService, FormService, NotificationService } from '@trex/core';
import { ModalService } from '@trex/shared';
import { BaseDetails } from '../../base/base-details';

@Component({
  selector: 'app-expense-type-details',
  templateUrl: './expense-type-details.component.html',
  styleUrls: ['./expense-type-details.component.scss']
})
export class ExpenseTypeDetailsComponent extends BaseDetails implements OnInit, OnDestroy {

  formConfig = {
    controls: [
      { name: 'name', validators: Validators.required },
      { name: 'isVariable', validators: Validators.required }
    ]
  };

  constructor(
    protected _service: UsersExpenseTypesService,
    protected _formService: FormService,
    protected _modalService: ModalService,
    protected _notificationService: NotificationService
  ) { 
    super(_service, _formService, _modalService, _notificationService);
  }
}
