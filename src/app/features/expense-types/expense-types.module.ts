import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SharedModule } from '@trex/shared';

import { ExpenseTypesRoutingModule } from './expense-types-routing.module';
import { ExpenseTypesComponent } from './expense-types-component/expense-types.component';
import { ExpenseTypeDetailsComponent } from './expense-type-details/expense-type-details.component';

const COMMON_DIRECTIVES = [
  ExpenseTypesComponent,
  ExpenseTypeDetailsComponent,
];

const ENTRY_COMPONENTS = [
  ExpenseTypeDetailsComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ExpenseTypesRoutingModule,
    SharedModule,
    FontAwesomeModule,
  ],
  declarations: COMMON_DIRECTIVES,
  entryComponents: ENTRY_COMPONENTS,
})
export class ExpenseTypesModule { }
