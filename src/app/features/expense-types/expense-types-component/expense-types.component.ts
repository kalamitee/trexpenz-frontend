import { Component, OnInit } from '@angular/core';
import { zip } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import * as _ from 'lodash';

import { UsersExpenseTypesService, UsersExpensesService } from '@trex/core';
import { ModalService } from '@trex/shared';
import { BaseParent } from '../../base/base-parent';

import { ExpenseTypeDetailsComponent } from '../expense-type-details/expense-type-details.component';

@Component({
  selector: 'app-expense-types',
  templateUrl: './expense-types.component.html',
  styleUrls: ['./expense-types.component.scss'],
  providers: [ModalService],
})
export class ExpenseTypesComponent extends BaseParent implements OnInit {

  isDeleting = false;
  hasData = false;

  modalConfig = {
    title: 'Expense Type',
    component: ExpenseTypeDetailsComponent
  };

  data = {
    isVariable: false
  };

  expenses$ = this._usersExpensesService.data$;
  expenseType$ = this._service.data$.pipe(
    tap(res => {
      if (!res.length) {
        this.isDeleting = false;
      }
    })
  );

  expenseTypeExpenses$ = zip(this.expenses$, this.expenseType$).pipe(
    map(([expenses, expenseTypes]) => {
      return { expenses, expenseTypes };
    })
  );

  constructor(
    protected _modalService: ModalService,
    protected _service: UsersExpenseTypesService,
    protected _usersExpensesService: UsersExpensesService,
  ) { 
    super(_modalService, _service);
  }

  ngOnInit() {
    super.ngOnInit();
    this._usersExpensesService.read();
  }

  showExpenses(event, expenseType) {
    event.stopPropagation();
  }
}
