import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { UsersService, AuthService } from '@trex/core';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, OnDestroy {

  private _unsubscribe$ = new Subject<void>();

  private _usersData$ = this._usersService.data$.pipe(
    takeUntil(this._unsubscribe$)
  ).subscribe(res => {
    this._authService.setUserConfig(res[0]);
    this._router.navigate(['app']);
  });

  constructor(
    private _router: Router,
    private _usersService: UsersService,
    private _authService: AuthService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this._unsubscribe$.next();
    this._unsubscribe$.complete();
  }

  cancel(): void {
    this._router.navigate(['/']);
  }

  register(data) {
    this._usersService.emptyDataStore();
    this._usersService.save(data);
  }
}
