import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { SharedModule } from '@trex/shared';

import { RegisterComponent } from './register-component/register.component';
import { RegisterRoutingModule } from './register-routing.module';

const COMMON_DIRECTIVES = [
  RegisterComponent
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    RegisterRoutingModule,
    SharedModule
  ],
  declarations: COMMON_DIRECTIVES,
})
export class RegisterModule { }
