import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd, RouteConfigLoadStart, RouteConfigLoadEnd } from '@angular/router';
import { Subject, fromEvent } from 'rxjs';
import { takeUntil, filter } from 'rxjs/operators';
import { NgProgress, NgProgressRef } from '@ngx-progressbar/core';

import { AppReuseStrategy } from '@trex/app-reuse-strategy';
import { AuthService, LogoutService } from '@trex/core';

@Component({
  selector: 'app-app-root',
  templateUrl: './app-root.component.html',
  styleUrls: ['./app-root.component.scss']
})
export class AppRootComponent implements OnInit, OnDestroy {

  private _unsubscribe$: Subject<void> = new Subject();
  private _documentClick$ = fromEvent(document, 'click');
  private logout$ = this._authService.data$.pipe(
    takeUntil(this._unsubscribe$)
  );
  
  private _activeMenu;
  private _routerProgressBar: NgProgressRef;

  subscription;
  active;
  
  loggedUser = JSON.parse(localStorage.getItem('user'));
  isLoading = false;
  menuConfig = {
    menu: [
      { label: 'Dashboard', routerLink: 'dashboard', icon: ['fas', 'tachometer-alt'] },
      { label: 'Budgets', routerLink: 'budgets', icon: ['fas', 'wallet'] },
      { label: 'Expenses', routerLink: 'expenses', icon: ['fas', 'money-bill-wave'] },
      { label: 'Expense Types', routerLink: 'expense-types', icon: ['fas', 'list-alt'] },
      { 
        label: 'My Account',
        icon: ['fas', 'user-circle'],
        isSubmenu: true, 
        menu: [
          { label: 'Account Details', routerLink: 'update-user', clickFn: function() { AppReuseStrategy.willReuse = true } },
          { label: 'Change Password', routerLink: 'change-password', clickFn: function() { AppReuseStrategy.willReuse = true } },
          { label: 'Logout', clickFn: () => { this.logout(); } },
        ] 
      }
    ]
  };

  constructor(
    private _router: Router,
    private _authService: AuthService,
    private _logoutService: LogoutService,
    private _ngProgressService: NgProgress,
  ) { 
    this._router.events.pipe(
      filter(res => {
        return res instanceof NavigationEnd || 
                res instanceof RouteConfigLoadStart ||
                res instanceof RouteConfigLoadEnd;
      }),
      takeUntil(this._unsubscribe$)
    ).subscribe((res: NavigationEnd | RouteConfigLoadEnd | RouteConfigLoadStart) => {
      if (res instanceof NavigationEnd) {
        this.isLoading = false;
        this.setActivatedMenu(res.url);
      } else if (res instanceof RouteConfigLoadStart) {
        if (this._routerProgressBar) {
          this._routerProgressBar.start();
        }

        this.isLoading = true;
      } else {
        if (this._routerProgressBar) {
          this._routerProgressBar.complete();
        }

        this.isLoading = false;
      }
    });
  }

  ngOnInit() {
    this._routerProgressBar = this._ngProgressService.ref('route');    
    this.logout$.subscribe(res => {
      this._logoutService.loggedOut();
      this._authService.emptyDataStore();
      this._authService.removeUserConfig();
      this._router.navigate(['']);
    });
  }

  ngOnDestroy() {
    this._unsubscribe$.next();
    this._unsubscribe$.complete();
  }

  setActivatedMenu(url) {
    const splittedUrl = url.split('/');
    const selectedLocation = splittedUrl[2] || 'dashboard';
    const foundMenuItem = this.menuConfig.menu.find((menuItem) => menuItem.routerLink === selectedLocation);

    if (foundMenuItem) {
      this.setActiveMenu(foundMenuItem);
    }
  }

  logout() {
    this._authService.delete();
  }

  preserveRoute() {
    AppReuseStrategy.willReuse = true;
  }

  toggleMenu(menuItem) {
    menuItem.active = !menuItem.active;

    if (menuItem.active) {
      setTimeout(() => {
        this.subscription = this._documentClick$.subscribe(res => {
          this.toggleMenu(menuItem);
        });
      });
    } else {
      this.subscription.unsubscribe();
    }
  }

  setActiveMenu(menuItem, callback = null) {
    if (this._activeMenu) {
      this._activeMenu.active = false;
    }

    menuItem.active = true;
    this._activeMenu = menuItem;

    if (callback) {
      callback();
    }
  }
}
