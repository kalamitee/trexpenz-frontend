import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AppRootComponent } from './app-root-component/app-root.component';


const routes: Routes = [
  {
    path: '',
    component: AppRootComponent,
    children: [
      { 
        path: 'dashboard', 
        loadChildren: () => import('../dashboard/dashboard.module').then(mod => mod.DashboardModule),
        data: { preload: true }
      },
      { 
        path: 'budgets', 
        loadChildren: () => import('../budgets/budgets.module').then(mod => mod.BudgetsModule) 
      },
      { 
        path: 'expenses', 
        loadChildren: () => import('../expenses/expenses.module').then(mod => mod.ExpensesModule) 
      },
      { 
        path: 'expense-types', 
        loadChildren: () => import('../expense-types/expense-types.module').then(mod => mod.ExpenseTypesModule) 
      },
      { 
        path: 'update-user', 
        loadChildren: () => import('../update-user/update-user.module').then(mod => mod.UpdateUserModule) 
      },
      { 
        path: 'change-password', 
        loadChildren: () => import('../change-password/change-password.module').then(mod => mod.ChangePasswordModule) 
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'dashboard',
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AppRootRoutingModule { }
