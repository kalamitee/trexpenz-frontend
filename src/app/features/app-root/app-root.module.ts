import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { AppRootRoutingModule } from './app-root-routing.module';

import { AppRootComponent } from './app-root-component/app-root.component';

const COMMON_DIRECTIVES = [
  AppRootComponent
];

@NgModule({
  imports: [
    CommonModule,
    FontAwesomeModule,
    AppRootRoutingModule,
  ],
  declarations: COMMON_DIRECTIVES,
})
export class AppRootModule { }
