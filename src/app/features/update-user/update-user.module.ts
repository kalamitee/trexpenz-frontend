import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '@trex/shared';

import { UpdateUserComponent } from './update-user-component/update-user.component';
import { UpdateUserRoutingModule } from './update-user-routing.module';

const COMMON_DIRECTIVES = [
  UpdateUserComponent
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    UpdateUserRoutingModule
  ],
  declarations: COMMON_DIRECTIVES,
  exports: COMMON_DIRECTIVES
})
export class UpdateUserModule { }
