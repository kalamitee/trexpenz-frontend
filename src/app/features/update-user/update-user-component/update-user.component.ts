import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { AppReuseStrategy } from '@trex/app-reuse-strategy';
import { UsersService } from '@trex/core';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.scss']
})
export class UpdateUserComponent implements OnInit, OnDestroy {

  private _unsubscribe$ = new Subject<void>();

  data = JSON.parse(localStorage.getItem('user'));
  data$ = this._usersService.data$.pipe(
    takeUntil(this._unsubscribe$)
  );

  constructor(
    private _router: Router,
    private _usersService: UsersService
  ) { }

  ngOnInit() {
  }

  ngOnDestroy() {
    this._unsubscribe$.next();
    this._unsubscribe$.complete();
  }

  update(updatedData) {
    this._usersService.update(updatedData);
    this.data$.subscribe(res => {
      localStorage.setItem('user', JSON.stringify(updatedData));
      this.navigateToDashBoard();
    });
  }

  cancel() {
    this.navigateToDashBoard();
  }
  
  private navigateToDashBoard() {
    AppReuseStrategy.willReuse = true;
    this._router.navigate(['app/dashboard']);
  }
}
