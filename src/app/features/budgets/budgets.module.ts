import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SharedModule } from '@trex/shared';

import { BudgetsRoutingModule } from './budgets-routing.module';
import { BudgetsComponent } from './budgets-component/budgets.component';
import { BudgetDetailsComponent } from './budget-details/budget-details.component';

const COMMON_DIRECTIVES = [
  BudgetsComponent,
  BudgetDetailsComponent,
];

const ENTRY_COMPONENTS = [
  BudgetDetailsComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    BudgetsRoutingModule,
    FontAwesomeModule,
  ],
  declarations: COMMON_DIRECTIVES,
  entryComponents: ENTRY_COMPONENTS,
})
export class BudgetsModule { }
