import { Component, OnInit } from '@angular/core';
import { map, tap } from 'rxjs/operators';
import * as _ from 'lodash';

import { Utils } from '@trex/shared';
import { UsersBudgetsService } from '@trex/core';
import { ModalService } from '@trex/shared';
import { BaseParent } from '../../base/base-parent';

import { BudgetDetailsComponent } from '../budget-details/budget-details.component';

@Component({
  selector: 'app-budgets',
  templateUrl: './budgets.component.html',
  styleUrls: ['./budgets.component.scss'],
  providers: [ModalService],
})
export class BudgetsComponent extends BaseParent implements OnInit {
  
  private _numericMonths = _.keys(Utils.MONTHS);

  isDeleting = false;
  hasData = false;

  modalConfig = {
    title: 'Budget',
    component: BudgetDetailsComponent
  };
  
  data = {
    budgetBreakdown: this._numericMonths.map((month) => {
      return {
        month: month,
        amount: null
      };
    })
  };

  budgets$ = this._service.data$.pipe(
    tap(res => {
      if (!res.length) {
        this.isDeleting = false;
      }
      
      this.hasData = Boolean(res.length);
    }),
    map(res => {
      return _.orderBy(res, ['year'], ['desc']);
    })
  );

  constructor(
    protected _modalService: ModalService,
    protected _service: UsersBudgetsService
  ) { 
    super(_modalService, _service);
  }
}
