import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormArray, Validators } from '@angular/forms';
import * as _ from 'lodash';

import { Utils } from '@trex/shared';
import { UsersBudgetsService, FormService, NotificationService } from '@trex/core';
import { ModalService } from '@trex/shared';
import { BaseDetails } from '../../base/base-details';

@Component({
  selector: 'app-budget-details',
  templateUrl: './budget-details.component.html',
  styleUrls: ['./budget-details.component.scss']
})
export class BudgetDetailsComponent extends BaseDetails implements OnInit, OnDestroy {

  formConfig = {
    controls: [
      { name: 'name', validators: Validators.required },
      { name: 'year', validators: Validators.required },
      { 
        name: 'amount',
        valueChanges: this.computeTotal.bind(this), 
        validators: Validators.required 
      },
      { 
        name: 'budgetBreakdown',
        valueChanges: this.computeTotal.bind(this),
        type: 'array',
        controls: [
          { name: 'amount' }
        ]
      }
    ]
  };

  months = Utils.MONTHS;
  budgetWarning = false;

  constructor(
    protected _service: UsersBudgetsService,
    protected _formService: FormService,
    protected _modalService: ModalService,
    protected _notificationService: NotificationService
  ) { 
    super(_service, _formService, _modalService, _notificationService);
  }

  transformDataForService(data) {
    data.budgetBreakdown = data.budgetBreakdown.map((breakdown) => {
      breakdown.amount = Number(breakdown.amount);
      return breakdown;
    });

    return data;
  }

  computeTotal() {
    const breakdownTotal = this.getTotalBudgetBreakdown();
    const budgetAmount = this.detailsForm.get('amount').value;

    if (breakdownTotal > budgetAmount) {
      this.budgetWarning = true;
    }

    else this.budgetWarning = false;
  }
  
  setAmountToBreakdownTotal() {
    const breakdownTotal = this.getTotalBudgetBreakdown();
    this.detailsForm.get('amount').patchValue(breakdownTotal);
  }

  getTotalBudgetBreakdown() {
    const budgetBreakdown = this.budgetBreakdown.value;

    return _.sumBy(budgetBreakdown, (bb: any) => {
      return Number(bb.amount);
    });
  }

  get budgetBreakdown() {
    return this.detailsForm.get('budgetBreakdown') as FormArray;
  }
}
