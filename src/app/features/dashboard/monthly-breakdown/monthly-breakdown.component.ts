import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';

import { Utils } from '@trex/shared';
import { DashboardUtils } from '../utils/utils';

@Component({
  selector: 'app-monthly-breakdown',
  templateUrl: './monthly-breakdown.component.html',
  styleUrls: ['./monthly-breakdown.component.scss']
})
export class MonthlyBreakdownComponent implements OnInit, OnChanges {

  @Input() budgetBreakdown;
  @Input() expenses;
  @Input() expenseTypes;
  @Input() selectedMonth;

  @Output() nextMonthClick = new EventEmitter();
  @Output() prevMonthClick = new EventEmitter();

  configuredExpenseTypes;
  totalExpensesAmount;
  remainingAmount;
  budgetStatus;

  months = Utils.MONTHS;

  constructor() { }

  ngOnInit() {
    this.init();
  }

  ngOnChanges(changes) {
    if (changes.expenses && !changes.expenses.firstChange) {
      this.init();
    }
  }

  init() {
    const budgetBreakdownAmount = _.get(this.budgetBreakdown, 'amount', null);

    this.configuredExpenseTypes = DashboardUtils.groupExpensesByExpenseTypes(this.expenses, this.expenseTypes);
    this.totalExpensesAmount = DashboardUtils.getTotalExpensesAMount(this.expenses);
    this.remainingAmount = DashboardUtils.getRemainingAmount(budgetBreakdownAmount, this.totalExpensesAmount);
    this.budgetStatus = DashboardUtils.getStatus(this.totalExpensesAmount, budgetBreakdownAmount);
  }

  nextMonth() {
    this.nextMonthClick.emit();
  }

  prevMonth() {
    this.prevMonthClick.emit();
  }
}
