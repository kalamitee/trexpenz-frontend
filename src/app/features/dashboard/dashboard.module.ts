import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { DashboardRoutingModule } from './dashboard-routing.module';

import { DashboardComponent } from './dashboard-component/dashboard.component';
import { BudgetSummaryComponent } from './budget-summary/budget-summary.component';
import { MonthlyBreakdownComponent } from './monthly-breakdown/monthly-breakdown.component';

const COMMON_DIRECTIVES = [
  DashboardComponent,
  BudgetSummaryComponent,
  MonthlyBreakdownComponent,
];

@NgModule({
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FontAwesomeModule,
  ],
  declarations: COMMON_DIRECTIVES,
  exports: COMMON_DIRECTIVES
})
export class DashboardModule { }
