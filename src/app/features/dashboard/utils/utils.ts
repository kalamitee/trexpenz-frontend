import * as _ from 'lodash';

export class DashboardUtils {

  static groupExpensesByExpenseTypes(expenses, expenseTypes) {
    const clonedExpenseTypes = _.cloneDeep(expenseTypes);
    const expensesGroup = _.groupBy(expenses, 'expenseType.id');
    const expensesSumByDate = _.mapValues(expensesGroup, (value) => {
      return _.sumBy(value, 'amount');
    });

    const expensesSumByDateValues = _.values(expensesSumByDate);
    const maxExpenseValue = _.max(expensesSumByDateValues);
    const minExpenseValue = _.min(expensesSumByDateValues);

    const configuredExpenseTypes = clonedExpenseTypes.map(etItem => {
      etItem.totalExpensesAmount = expensesSumByDate[etItem.id] || 0;
      DashboardUtils.setExpenseTypeTag(maxExpenseValue, minExpenseValue, etItem);
      return etItem;
    });

    return configuredExpenseTypes;
  }

  static setExpenseTypeTag(max, min, expenseType) {
    if (max !== min) {
      switch(expenseType.totalExpensesAmount) {
        case max:
          expenseType.tag = 'max';
          break;
        case min:
          expenseType.tag = 'min';
          break;
      }
    }
  }

  static getRemainingAmount(totalAmount, totalExpensesAmount) {
    if (totalAmount === 'N/A') {
      return 'N/A';
    }

    const diff = totalAmount - totalExpensesAmount;
    
    return diff < 0 ? 0 : diff;
  }

  static getTotalExpensesAMount(expenses) {
    return _.sumBy(expenses, (expense: any) => expense.amount);
  }

  static getStatus(totalExpensesAmount, totalBudgetAmount) {
    const spentPercentage = totalExpensesAmount / totalBudgetAmount;

    if (!totalBudgetAmount || isNaN(spentPercentage)) {
      return 'neutral';
    } else if (spentPercentage <= 0.7) {
      return 'healthy';
    } else if (spentPercentage <= 1) {
      return 'danger';
    } else {
      return 'overspending';
    }
  }
}
