import { Component, OnInit } from '@angular/core';
import { zip } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import * as _ from 'lodash';

import { UsersBudgetsService, UsersExpensesService, UsersExpenseTypesService } from '@trex/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  selectedYear = null;
  selectedMonth = null;

  selectedBudget = null;
  selectedExpenses = [];
  selectedBudgetBreakdown = null;
  
  budgets$ = this._usersBudgetsService.data$;
  expenses$ = this._usersExpensesService.data$;
  expenseTypes$ = this._usersExpenseTypesService.data$;

  budgetExpenses$ = zip(this.budgets$, this.expenses$, this.expenseTypes$).pipe(
    map(([budgets, expenses, expenseTypes]) => {
      const sortedExpenseTypes = _.orderBy(expenseTypes, [(o) => o.name.toLowerCase()], ['asc']);
      const clonedExpenseTypes = _.cloneDeep(sortedExpenseTypes);
      this.addDummyExpenseType(clonedExpenseTypes);
      return { budgets, expenses, expenseTypes: clonedExpenseTypes };
    }),
    tap(res => {
      this.selectedBudget = this.yearBudget(res.budgets);
      this.setMonthlyData(res.expenses);
    })
  );

  constructor(
    private _usersBudgetsService: UsersBudgetsService,
    private _usersExpensesService: UsersExpensesService,
    private _usersExpenseTypesService: UsersExpenseTypesService
  ) { 
    this.selectedYear = new Date().getFullYear();
    this.selectedMonth = new Date().getMonth() + 1;
  }

  ngOnInit() {
    this.readBudgetExpenses();
  }

  addDummyExpenseType(expenseTypes) {
    let dummyExpenseTypeFound = expenseTypes.find(expenseType => expenseType.id === 'undefined');
    if (!dummyExpenseTypeFound && expenseTypes.length) {
      expenseTypes.push({ id: 'undefined', name: '---' });
    }
  }

  setMonthlyData(expenses) {
    this.selectedBudgetBreakdown = this.monthBudgetBreakdown();
    this.selectedExpenses = this.monthExpenses(expenses);
  }

  yearBudget(budgets) {
    return budgets.find(budget => budget.year === this.selectedYear);
  }

  monthBudgetBreakdown() {
    if (this.selectedBudget) {
      return this.selectedBudget.budgetBreakdown.find((bbItem) => bbItem.month === this.selectedMonth);
    }
    
    return null;
  }

  monthExpenses(expenses) {
    return expenses.filter(expense => new Date(expense.date).getMonth() + 1 === this.selectedMonth);
  }

  nextYear() {
    this.selectedYear += 1;
    this.readBudgetExpenses();
  }

  prevYear() {
    this.selectedYear -= 1;
    this.readBudgetExpenses();
  }

  nextMonth(expenses) {
    if (this.selectedMonth < 12) {
      this.selectedMonth += 1;
      this.setMonthlyData(expenses);
    }
  }

  prevMonth(expenses) {  
    if (this.selectedMonth > 1) {  
      this.selectedMonth -= 1;
      this.setMonthlyData(expenses);
    }
  }

  readBudgetExpenses() {
    this._usersBudgetsService.readDataFromCache();
    this._usersExpenseTypesService.readDataFromCache();
    this.readExpenses();
  }

  readExpenses() {
    const query = this.getQuery();
    this._usersExpensesService.read(query);
  }

  getQuery() {
    const firstDayOfYear = `${this.selectedYear}-01-01`;
    const lastDayOfYear = `${this.selectedYear}-12-31`;
    const q = {
      query: {
        op: 'and',
        conditions: [
          {
            prop: 'date',
            value: firstDayOfYear,
            op: 'ge'
          },
          {
            prop: 'date',
            value: lastDayOfYear,
            op: 'le'
          }
        ]
      }
    };

    return JSON.stringify(q);
  }
}
