import { Component, OnInit, OnChanges, Input, Output, EventEmitter } from '@angular/core';
import * as _ from 'lodash';

import { DashboardUtils } from '../utils/utils';

@Component({
  selector: 'app-budget-summary',
  templateUrl: './budget-summary.component.html',
  styleUrls: ['./budget-summary.component.scss']
})
export class BudgetSummaryComponent implements OnInit, OnChanges {

  @Input() budget;
  @Input() expenses;
  @Input() expenseTypes;
  @Input() selectedYear;

  @Output() prevYearClick = new EventEmitter();
  @Output() nextYearClick = new EventEmitter();

  totalExpensesAmount;
  totalBudgetAmount;
  remainingBudgetAmount;
  budgetName;
  configuredExpenseTypes;

  budgetStatus;

  constructor() { }

  ngOnInit() {
    this.init();
  }

  ngOnChanges(changes) {
    if (changes.expenses && !changes.expenses.firstChange) {
      this.init();
    }
  }

  private init() {
    this.totalExpensesAmount = this.getExpenseTotal(this.expenses);
    this.totalBudgetAmount = this.getBudgetAmount(this.budget);
    this.remainingBudgetAmount = DashboardUtils.getRemainingAmount(this.totalBudgetAmount, this.totalExpensesAmount);
    this.budgetStatus = DashboardUtils.getStatus(this.totalExpensesAmount, this.totalBudgetAmount);
    this.budgetName = this.getBudgetName(this.budget);
    this.configuredExpenseTypes = DashboardUtils.groupExpensesByExpenseTypes(this.expenses, this.expenseTypes);
  }

  private getBudgetName(budget) {
    if (_.isNil(budget)) {
      return 'N/A';
    }

    return budget.name;
  }

  private getBudgetAmount(budget) {
    if (_.isNil(budget)) {
      return 'N/A';
    }

    return budget.amount;
  }

  private getExpenseTotal(expenses) {
    return _.sumBy(expenses, 'amount');
  }

  private getRemainingBudgetAmount(totalBudgetAmount, totalExpensesAmount) {
    if (totalBudgetAmount === 'N/A') {
      return 'N/A';
    }

    const diff = totalBudgetAmount - totalExpensesAmount;
    
    return diff < 0 ? 0 : diff;
  }

  prevYear() {
    this.prevYearClick.emit();
  }

  nextYear() {
    this.nextYearClick.emit();
  }
}
