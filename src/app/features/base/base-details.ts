import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil, tap } from 'rxjs/operators';
import * as _ from 'lodash';

import { FormService, BaseService, NotificationService } from '@trex/core';
import { ModalService } from '@trex/shared';

export class BaseDetails {

  private _unSubscribe$: Subject<void> = new Subject();

  detailsForm: FormGroup;
  formConfig: any;

  data: any;
  data$ = this._service.data$.pipe(
    // tap(res => this._notificationService.success('Saved')),
    takeUntil(this._unSubscribe$)
  ).subscribe(res => this._modalService.close());

  constructor(
    protected _service: BaseService,
    protected _formService: FormService,
    protected _modalService: ModalService,
    protected _notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.detailsForm = this._formService.generateFormGroup(this.formConfig, this.data);
  }

  ngOnDestroy() {
    this._unSubscribe$.next();
    this._unSubscribe$.complete();
  }
  
  saveData(formData) {
    if (this.detailsForm.valid) {
      const updatedData = _.merge(this.data, formData);
      const transformedData = this.transformDataForService(updatedData);
  
      this._service.save(transformedData);
    }
  }

  setData(data) {
    this.data = this.transformDataForForm(data);
  }

  closeModal() {
    this._modalService.close();
  }

  transformDataForService(data) {
    return data;
  }

  transformDataForForm(data) {
    return data;
  }
}
