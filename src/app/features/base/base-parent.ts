import * as _ from 'lodash';

import { BaseService } from '@trex/core';
import { ModalService } from '@trex/shared';

export class BaseParent {

  q = null;

  modalConfig = {
    title: null,
    component: null,
  };

  constructor(
    protected _modalService: ModalService,
    protected _service: BaseService
  ) {}

  ngOnInit() {
    this._service.read(this.q);
    this._modalService.setConfig(this.modalConfig);
  }

  delete(event, data) {
    event.stopPropagation();
    this._service.delete(data.id);
  }

  openModal(data) {
    this._modalService.setModalData(_.cloneDeep(data));
    this._modalService.open();
  }
}
