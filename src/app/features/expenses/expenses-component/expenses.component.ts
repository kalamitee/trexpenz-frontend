import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import * as _ from 'lodash';

import { UsersExpensesService } from '@trex/core';
import { ModalService } from '@trex/shared';
import { BaseParent } from '../../base/base-parent';

import { ExpenseDetailsComponent } from '../expense-details/expense-details.component';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.scss'],
  providers: [ModalService],
})
export class ExpensesComponent extends BaseParent implements OnInit {

  selectedDate = moment().format('YYYY-MM-DD');
  isDeleting = false;

  modalConfig = {
    title: 'Expense',
    component: ExpenseDetailsComponent,
  };

  data = {};

  // showExpenseList = false;
  selectedExpensesForDay;

  expenses$ = this._service.data$.pipe(
    map(res => {
      const expensesByDate = _.groupBy(res, 'date');
      const expensesSumByDate = _.mapValues(expensesByDate, (value) => {
        return _.sumBy(value, 'amount');
      });
      
      this.setSelectedExpensesByDate(expensesByDate, this.selectedDate);
      return { byDate: expensesByDate, sumByDate: expensesSumByDate };
    })
  );

  constructor(
    protected _modalService: ModalService,
    protected _service: UsersExpensesService,
  ) { 
    super(_modalService, _service);

    const currentYear = new Date().getFullYear();
    this.updateQuery({ start: `${currentYear}-01-01`, end: `${currentYear}-12-31` });
  }

  private updateQuery(dateRange: { start: string, end: string }) {
    const filter = {
      query: {
        op: 'and',
        conditions: [
          {
            prop: 'date',
            value: dateRange.start,
            op: 'ge'
          },
          {
            prop: 'date',
            value: dateRange.end,
            op: 'le'
          }
        ]
      }
    };

    this.q = JSON.stringify(filter);
  }

  setListData(expenses, date) {
    this.selectedDate = date.mDate.format('YYYY-MM-DD');
    this.setSelectedExpensesByDate(expenses, this.selectedDate);
  }
  
  private setSelectedExpensesByDate(expenses, date) {
    this.selectedExpensesForDay = expenses[date];
  }

  updateCalendar(year) {
    this.updateQuery({ start: `${year}-01-01`, end: `${year}-12-31` });
    this._service.read(this.q);
  }

  nextDate(expenses) {
    const newDate = moment(this.selectedDate).add(1, 'days');
    this.updateExpenses(newDate, expenses);
  }

  prevDate(expenses) {
    const newDate = moment(this.selectedDate).subtract(1, 'days');
    this.updateExpenses(newDate, expenses);
  }

  onDateChange(updatedDate, expenses) {
    const newDate = updatedDate.mDate;
    this.updateExpenses(newDate, expenses);
  }

  updateExpenses(newDate, expenses) {
    const oldDate = this.selectedDate;

    this.selectedDate = newDate.format('YYYY-MM-DD');

    if (!this.isSameYear(oldDate, newDate)) {
      this.updateCalendar(newDate.year());
    } else {
      this.setSelectedExpensesByDate(expenses, this.selectedDate);
    }
  }

  isSameYear(oldDate, newDate) {
    return moment(oldDate).isSame(newDate.format('YYYY-MM-DD'), 'year');
  }

  deleteExpense(event, data) {
    event.stopPropagation();
    this._service.delete(data.id);
  }
}
