import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExpenseSidebarComponent } from './expense-sidebar.component';

describe('ExpenseSidebarComponent', () => {
  let component: ExpenseSidebarComponent;
  let fixture: ComponentFixture<ExpenseSidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExpenseSidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExpenseSidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
