import { Component, OnInit, OnChanges, Input, TemplateRef, Output, EventEmitter } from '@angular/core';
import { fromEvent } from 'rxjs';
import * as _ from 'lodash';

@Component({
  selector: 'app-expense-sidebar',
  templateUrl: './expense-sidebar.component.html',
  styleUrls: ['./expense-sidebar.component.scss']
})
export class ExpenseSidebarComponent implements OnInit, OnChanges {

  private _documentClick$ = fromEvent(document, 'click');

  calendarActive;
  subscription;
  totalExpenses;
  actionCancellable = false;

  @Input() listDataTemplate: TemplateRef<any>;
  @Input() sidebarData;
  @Input() date;
  
  @Output() expenseClick = new EventEmitter();
  @Output() cancel = new EventEmitter();
  @Output() delete = new EventEmitter();
  @Output() dateChange = new EventEmitter();
  @Output() nextClick = new EventEmitter();
  @Output() prevClick = new EventEmitter();

  constructor() { }

  ngOnInit() {
    this.setTotalExpenses();
  }

  ngOnChanges(changes) {
    if (changes.sidebarData && changes.sidebarData.currentValue && !changes.sidebarData.firstChange) {
      this.setTotalExpenses();
    }
  }

  setTotalExpenses() {
    this.totalExpenses = _.sumBy(this.sidebarData, 'amount');
  }

  prev() {
    this.prevClick.emit();
  }

  next() {
    this.nextClick.emit();
  }

  toggleCalendar() {
    this.calendarActive = !this.calendarActive;

    if (this.calendarActive) {
      setTimeout(() => {
        this.subscription = this._documentClick$.subscribe(res => {
          this.toggleCalendar();
        });
      });
    } else {
      this.subscription.unsubscribe();
    }
  }

  triggerDelete() {
    this.delete.emit();
    this.actionCancellable = true;
  }

  triggerCancel() {
    this.cancel.emit();
    this.actionCancellable = false;
  }
}
