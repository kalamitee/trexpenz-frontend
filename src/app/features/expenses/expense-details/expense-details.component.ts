import { Component, OnInit, OnDestroy } from '@angular/core';
import { Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { tap , map} from 'rxjs/operators';
import * as _ from 'lodash';

import { UsersExpensesService, FormService, UsersExpenseTypesService, NotificationService } from '@trex/core';
import { ModalService } from '@trex/shared';
import { BaseDetails } from '../../base/base-details';

@Component({
  selector: 'app-expense-details',
  templateUrl: './expense-details.component.html',
  styleUrls: ['./expense-details.component.scss']
})
export class ExpenseDetailsComponent extends BaseDetails implements OnInit, OnDestroy {

  private _expenseTypes = [];
  columnBreakpoint;
  
  formConfig = {
    controls: [
      { name: 'date', validators: Validators.required },
      { name: 'name', validators: Validators.required },
      { name: 'amount', validators: Validators.required },
      { name: 'expenseType', validators: Validators.required }
    ]
  };

  userExpenseTypes$ = this._usersExpenseTypesService.data$.pipe(
    map(res => {
      return _.orderBy(res, [(o) => o.name.toLowerCase()], ['asc']);
    }),
    tap(res => {
      this._expenseTypes = res;
      this.columnBreakpoint = Math.ceil(this._expenseTypes.length / 2);
    })
  );

  constructor(
    protected _service: UsersExpensesService,
    protected _formService: FormService,
    protected _modalService: ModalService,
    protected _notificationService: NotificationService,
    protected _usersExpenseTypesService: UsersExpenseTypesService,
    protected _router: Router,
  ) { 
    super(_service, _formService, _modalService, _notificationService);
  }

  ngOnInit() {
    super.ngOnInit();
    this._usersExpenseTypesService.readDataFromCache();
  }

  transformDataForForm(data) {
    if (data.expenseType) {
      data.expenseType = data.expenseType.id;
    }

    return data;
  }

  transformDataForService(data) {
    data.expenseType = this.getExpenseTypeById(data.expenseType);
    return data;
  }

  redirectToExpenseTypes() {
    this._modalService.close();
    this._router.navigate(['/app/expense-types']);
  }

  private getExpenseTypeById(expenseTypeId) {
    return this._expenseTypes.find(expenseType => expenseType.id === expenseTypeId);
  }
}
