import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SharedModule } from '@trex/shared';

import { ExpensesRoutingModule } from './expenses-routing.module';
import { ExpensesComponent } from './expenses-component/expenses.component';
import { ExpenseSidebarComponent } from './expense-sidebar/expense-sidebar.component';
import { ExpenseDetailsComponent } from './expense-details/expense-details.component';

const COMMON_DIRECTIVES = [
  ExpensesComponent,
  ExpenseSidebarComponent,
  ExpenseDetailsComponent,
];

const ENTRY_COMPONENTS = [
  ExpenseDetailsComponent,
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    ExpensesRoutingModule,
    FontAwesomeModule,
  ],
  declarations: COMMON_DIRECTIVES,
  entryComponents: ENTRY_COMPONENTS,
})
export class ExpensesModule { }
