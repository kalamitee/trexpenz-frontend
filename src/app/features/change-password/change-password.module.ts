import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ChangePasswordComponent } from './change-password-component/change-password.component';
import { ChangePasswordRoutingModule } from './change-password-routing.module';

const COMMON_DIRECTIVES = [
  ChangePasswordComponent
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    ChangePasswordRoutingModule
  ],
  declarations: COMMON_DIRECTIVES,
  exports: COMMON_DIRECTIVES
})
export class ChangePasswordModule { }
