import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AppReuseStrategy } from '@trex/app-reuse-strategy';
import { FormService, CredService } from '@trex/core';
import { matchedPassword } from '@trex/shared/utils';

@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit, OnDestroy {

  private _unsubscirbe$ = new Subject<void>();

  formConfig = {
    controls: [
      { name: 'password', validators: Validators.required },
      { name: '_confirmPassword', validators: Validators.required }
    ],
    validators: [matchedPassword]
  };

  detailsForm;
  data$ = this._credService.data$.pipe(
    takeUntil(this._unsubscirbe$)
  ).subscribe(res => {
    this._router.navigate(['app']);
  });

  constructor(
    private _router: Router,
    private _formService: FormService,
    private _credService: CredService
  ) { 
    this._credService.endpoint = 'cred';
  }

  ngOnInit() {
    this.detailsForm = this._formService.generateFormGroup(this.formConfig, {});
  }

  ngOnDestroy() {
    this._unsubscirbe$.next();
    this._unsubscirbe$.complete();
  }

  onSubmit(formValue) {
    if (this.detailsForm.valid) {
      this._credService.update({ password: formValue.password });
    }
  }

  onCancel() {
    this.navigateToDashBoard();
  }

  private navigateToDashBoard() {
    AppReuseStrategy.willReuse = true;
    this._router.navigate(['app/dashboard']);
  }
}
