import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { takeUntil, map } from 'rxjs/operators';
import { Subject } from 'rxjs';

import { AuthService } from '@trex/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  private _unSubscribe$: Subject<void> = new Subject();

  loginForm = this._formBuilder.group({
    username: ['', Validators.required],
    password: ['', Validators.required]
  });

  constructor(
    private _router: Router,
    private _formBuilder: FormBuilder,
    private _authService: AuthService
  ) { }

  ngOnInit() {
    this._authService.data$.pipe(
      map(res => res[0]),
      takeUntil(this._unSubscribe$)
    ).subscribe(res => {
      this.setCredentials(res);
      this._router.navigate(['/app/dashboard']);
    });
  }

  ngOnDestroy() {
    this._unSubscribe$.next();
    this._unSubscribe$.complete();
  }
  
  onSubmit(): void {
    if (this.loginForm.valid) {
      this._authService.login(this.loginForm.value);
    }
  }

  setCredentials(userData) {
    this._authService.setUserConfig(userData); 
  }

  goHome() {
    this._router.navigate(['/']);
  }
}
