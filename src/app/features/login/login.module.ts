import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '@trex/shared';

import { LoginComponent } from './login-component/login.component';
import { LoginRoutingModule } from './login-routing.module';

const COMMON_DIRECTIVES = [
  LoginComponent
];

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    SharedModule,
    LoginRoutingModule
  ],
  declarations: COMMON_DIRECTIVES,
})
export class LoginModule { }
