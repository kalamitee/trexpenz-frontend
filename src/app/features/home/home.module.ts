import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeComponent } from './home-component/home.component';
import { HomeRoutingModule } from './home-routing.module';

const COMMON_DIRECTIVES = [
  HomeComponent
];

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule
  ],
  declarations: COMMON_DIRECTIVES,
  exports: COMMON_DIRECTIVES
})
export class HomeModule { }
