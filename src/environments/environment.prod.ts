export const environment = {
  production: true,
  baseUrl: 'https://trexpenz-api.herokuapp.com/api/'
};
